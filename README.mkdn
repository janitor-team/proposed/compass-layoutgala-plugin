Compass Layout-gala plugin
==========================

Collection of 40 CSS layouts styling a single HTML markup, without hacks
nor workarounds and a good cross-browser compatibility.

The layouts uses techniques like negative margins, any order columns and
opposite floats.

<http://blog.html.it/layoutgala/>
<http://www.alistapart.com/articles/negativemargins/>
<http://www.positioniseverything.net/articles/onetruelayout/anyorder>


Installation
============

From the command line:

	sudo gem install compass-layoutgala-plugin

Add to a project (rails: compass.config, other: config.rb):

	require 'compass-layoutgala-plugin'

Or create a new project:

	compass create project_directory --using layoutgala


Copyright and Licensing
=======================

Copyright (C) 2011, 2017  Jonas Smedegaard <dr@jones.dk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
