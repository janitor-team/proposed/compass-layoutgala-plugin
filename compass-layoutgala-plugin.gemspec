Gem::Specification.new do |s|
  # Release Specific Information
  s.version = "0.2.1"
  s.date = "2017-01-23"

  # Gem Details
  s.name = "compas-layoutgala-plugin"
  s.authors = ["Jonas Smedegaard"]
  s.summary = %q{40 CSS base layouts for same HTML markup using Compass}
  s.description = %q{40 CSS base layouts for same HTML markup using Compass}
  s.email = "dr@jones.dk"
  s.homepage = "http://dr.jones.dk/"

  # Gem Files
  s.files = %w(README.mkdn)
  s.files += Dir.glob("lib/**/*.*")
  s.files += Dir.glob("stylesheets/**/*.*")
  s.files += Dir.glob("templates/**/*.*")

  # Gem Bookkeeping
  s.has_rdoc = false
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.3.6}
  s.add_dependency('sass', ['~>3.3'])
end
