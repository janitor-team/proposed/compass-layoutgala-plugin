# Make sure you list all the project template files here in the manifest.
stylesheet 'screen.scss', :media => 'screen, projection'
stylesheet '_base.scss'
image 'header.png'
html 'layoutgala.html'

description "Layout Gala: 40 CSS base layouts for same HTML markup"

help %Q{
Installs some html and a stylesheet that you can use directly
or refer to as an example.
}

welcome_message %Q{
Please refer to the layoutgala.html file to see how the markup should be structured.
And to the layoutgala stylesheet partial to see how to use the library and apply it to your markup.
}
